<?php 
function portfolio_taxonomy() {
//Biggest repeater - Check cpt
    if ( have_rows( 'portfolio_options', 'option' ) ) { while ( have_rows( 'portfolio_options', 'option' ) ) { the_row(); 
        $portfolio_cpt_theme = mb_strtolower(get_sub_field( 'name' ));
    //taxonomy cpt
        if ( have_rows( 'taxonomy_group', 'option' ) ) : while ( have_rows( 'taxonomy_group', 'option' ) ) : the_row(); 
        //with taxonomy
            if ( have_rows( 'item_taxonomy', 'option' ) ) : while ( have_rows( 'item_taxonomy', 'option' ) ) : the_row();
            //START DEFINE VARIABLE
                $nametaxonomy = get_sub_field( 'name_taxonomy' );
                $test = mb_strtolower($nametaxonomy);
                $search = explode(",","ą,ć,ę,ł,ń,ó,ś,ź,ż");
                $replace = explode(",","a,c,r,l,n,o,s,z,z");
                $urlTitle = str_replace($search, $replace, $test);


            //STOP DEFINE VARIABLE
                $labels = array(
                    'name' => _x( $nametaxonomy, 'taxonomy general name' ),
                    'singular_name' => _x( $nametaxonomy, 'taxonomy singular name' ),
                    'search_items' =>  __( 'Wyszukaj ' . $nametaxonomy ),
                    'all_items' => __( 'Wszystkie ' . $nametaxonomy ),
                    'edit_item' => __( 'Edytuj ' . $nametaxonomy ), 
                    'update_item' => __( 'Uaktualnij ' . $nametaxonomy ),
                    'add_new_item' => __( 'Dodaj ' . $nametaxonomy ),
                    'new_item_name' => __( 'Nowe ' . $nametaxonomy ),
                );    




                register_taxonomy($urlTitle,array($portfolio_cpt_theme), array(
                    'hierarchical' => true,
                    'labels' => $labels,
                    'show_ui' => true,
                    'menu_icon'   => 'dashicons-images-alt2',
                    'show_admin_column' => true,
                    'query_var' => true,
                    'rewrite' => array( 'slug' => $urlTitle ),
                ));
            endwhile; endif;
        endwhile; endif;
    } 
}
}


// Taksomia popularne
add_action( 'init', 'portfolio_taxonomy', 0 );