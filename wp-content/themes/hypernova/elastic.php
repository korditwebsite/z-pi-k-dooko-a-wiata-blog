<?php 
if ( have_rows( 'pole_powtarzalne' ) ) : while ( have_rows( 'pole_powtarzalne' ) ) : the_row(); 
	$selecttemplates = get_sub_field( 'select' );

//Inicjacja 1 elastic field
	if ($selecttemplates == 1) {
		if( have_rows('ulozenie_proste') ): while ( have_rows('ulozenie_proste') ) : the_row();
			get_template_part( 'templates/basic/init-templates'); 
		endwhile; endif;
	}

//Inicjacja 2 elastic field
	if ($selecttemplates == 2) {
		if( have_rows('CPT') ): while ( have_rows('CPT') ) : the_row();
			get_template_part( 'templates/cpt/init-templates'); 
		endwhile; endif;
	}

//Inicjacja 3 elastic field
	if ($selecttemplates == 3) {
		if( have_rows('Addons') ): while ( have_rows('Addons') ) : the_row();
			get_template_part( 'templates/addons/init-templates'); 
		endwhile; endif;
	}

//Inicjacja 4 elastic field
	if ($selecttemplates == 4) {
		if( have_rows('contact') ): while ( have_rows('contact') ) : the_row();
			get_template_part( 'templates/contact/init-templates'); 
		endwhile; endif;
	}

//Inicjacja 5 elastic field
	if ($selecttemplates == 5) {
		if( have_rows('hero') ): while ( have_rows('hero') ) : the_row();
			get_template_part( 'templates/hero/init-templates'); 
		endwhile; endif;
	}
//Inicjacja 5 elastic field
	if ($selecttemplates == 6) {
		if( have_rows('blog') ): while ( have_rows('blog') ) : the_row();
			get_template_part( 'templates/blog/init-templates'); 
		endwhile; endif;
	}
endwhile;  endif;