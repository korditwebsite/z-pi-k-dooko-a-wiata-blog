<?php
/**
* Template Name: CPT 
*/
?>
<?php get_header(); ?>
<section id="blog-page">
	<div class="container">
		<div class="row">
			<?php
			$namefield = get_field( 'slug_cpt' );
			$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
			$args = array(
				'post_type'   => $namefield,
				'post_status' => 'publish',
				'order' => 'ASC',
				'posts_per_page'=>'3',
				// 'tax_query' => [
				// 	[

				// 		'taxonomy' => 'rodzaj',
				// 		'field' => 'term_id',
				// 	]
				// ],
			);
			?>
			<?php $testimonials = new WP_Query( $args ); 
			if( $testimonials->have_posts() ) :
				?>
				<?php
				while( $testimonials->have_posts() ) :
					$testimonials->the_post();
					?>
					<div class="col-xl-4">
						<div class="inner-post-list">
							<div class="thumbnail">
								<?php the_post_thumbnail( 'category-thumb' ); ?>
							</div>
							<div class="inner-content">
								<div class="title">
									<?php the_title(); ?>
								</div>
								<?php if (has_excerpt() || get_the_content()) { ?>
									<div class="excerpt">
										<?php 
										if (has_excerpt()) {
											the_excerpt();
										} else {
											echo wp_trim_words( get_the_excerpt(), 20 );
										}
										?>
									</div>
								<?php } ?>
								<div class="link">
									<a href="<?php echo get_permalink($parent_id); ?>">
										<?php _e( 'read more', 'textdomain' ); ?>
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php
				endwhile;
				wp_reset_postdata();
				?>
				<?php
			else :
				esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
			endif;
			?>
		</div>
	</div>
</section>
<?php get_footer(); ?>