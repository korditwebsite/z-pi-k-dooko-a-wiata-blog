<?php 
get_header();
if ( have_rows( 'ustawnienia_footera', 'option' ) ) : while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row(); 
	if ( have_rows( 'telefony', 'option' ) ) : while ( have_rows( 'telefony', 'option' ) ) : the_row(); 
		$telefon = get_sub_field( 'telefon' );
	endwhile; endif;
endwhile; endif;
?>

<main id="single-oferta single-oferta-custom">
	<article>
		<div id="dla_kogo" class="container my-5">
			<div class="content-post bg-white">
				<div class="row">
					<div class="col-xl-4">
						<div class="thumbnail">
							<?php the_post_thumbnail( 'full', "", array( "class" => "img-fluid", "alt" => "realizacje" ) ); ?>
						</div>
					</div>
					<div class="col-xl-8">
						<div class="tresc">
							<div class="cena">
								Cena produktu: <b><?php the_field("cena"); ?></b>
							</div>
							<?php
							the_content();
							?>
						</div>
					</div>
					<div class="col-xl-12">
						<div class="cta mt-5 mb-3">
							<h3 class="w-100 text-ceter">
								Jesteś zainteresowany ofertą?
							</h3>
							<div class="container-button">
								<a href="tel:<?php echo $telefon; ?>">
									<button>Zadzwoń! <?php echo $telefon; ?></button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</main>
<?php get_footer(); ?>