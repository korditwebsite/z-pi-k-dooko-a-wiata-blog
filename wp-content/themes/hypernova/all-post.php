<section id="blog">
	<div class="row">
		<div class="col-xl-8">
			<div class="all-post">
				<div class="inner-alt-post">
					<?php 
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post(); 
							$parent_id = $post->post_parent;
							?>
							<div class="inner-post-list">
								<div class="row">
									<div class="col-md-5">
										<div class="thumbnail">
											<?php the_post_thumbnail( 'category-thumb' ); ?>
										</div>
									</div>
									<div class="col-xl-7">
										<div class="inner-content">
											<div class="row">
												<div class="col-9">
													<div class="categories">
														<?php echo get_the_category_list( ' ', ',', $term->ID ); ?>
													</div>
												</div>
												<div class="col-3">
													<span class="data">
														<b>
															<?php echo get_the_date( 'Y' ); ?>
														</b>
														<?php echo get_the_date( 'm-d' ); ?>
													</span>
												</div>
											</div>
											<h3>
												<?php the_title(); ?>
											</h3>

											<div class="excerpt">
												<?php 
												if (has_excerpt()) {
													the_excerpt();
												} else {
													echo wp_trim_words( get_the_excerpt(), 20 );
												}

												?>
											</div>
											<div class="link">
												<a href="<?php echo get_permalink($parent_id); ?>">
													<?php _e( 'Czytaj więcej', 'textdomain' ); ?>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } } ?>
					</div>
				</div>
			</div>
			<div class="col-xl-4">
				<aside>
					<?php echo get_sidebar(); ?>
				</aside>
			</div>
		</div>
	</section>