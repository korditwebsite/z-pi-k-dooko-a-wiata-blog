<?php get_header(); ?>
<main id="blog-page">
	<div class="container">
		<section id="blog">
			<div class="row">
				<div class="col-xl-12">
					<div class="all-post">
						<div class="inner-alt-post">
							<div class="inner-post-list">
								<div class="inner-content">
									<div class="row mb-4">
										<div class="col-xl-9">
											<div class="categories">
												<span>Kategorie:</span>
												<?php echo get_the_category_list( ' ', ',', $term->ID ); ?>
											</div>
											<div class="tags">
												<?php 
												$tags = get_tags();
												$html = '<div class="post_tags"><span>TAGI:</span> ';
												foreach ( $tags as $tag ) {
													$tag_link = get_tag_link( '| ', $tag->term_id );

													$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
													$html .= "{$tag->name}, </a>";
												}
												$html .= '</div>';
												echo '<div class="single-tag">'. $html . " </div>";
												?>
											</div>
										</div>
										<div class="col-xl-3">
											<span class="data">
												<?php echo get_the_date( 'Y m-d' ); ?>
											</span>
										</div>
									</div>
									<div class="description my-5">
										<?php 
										the_content();
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<?php get_footer(); ?>

