<?php get_header(3); ?>
<main id="blog-page">
	<div class="container">
		<?php get_template_part( 'all', 'post' ); ?>
	</div>
</main>
<?php get_footer(); ?>