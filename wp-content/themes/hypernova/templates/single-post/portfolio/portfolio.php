<?php
//Size container
if (!have_rows( 'size_container' )) {
	$datamobile = "12";
	$datatablet = "12";
	$datadesktop = "12";	
}
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
	$datamobile = get_sub_field( 'mobile' ); 
	$datatablet = get_sub_field( 'tablet' ); 
	$datadesktop = get_sub_field( 'desktop' ); 
endwhile; endif;

//var
$crop = get_sub_field( 'crop_thumbnail' );
$cropsizedata = get_sub_field( 'crop_to' );
$value = get_sub_field( 'value' );
$cropsize = "height:" . $cropsizedata . $value . ";";
$images = get_field('gallery');
$size = 'gallery-home';
$current_id_post = get_the_ID();
$size2 = 'hero_image';
if (get_field( 'height' )) {
	$height = get_field( 'height' );
} else {
	$height = "auto";
}


//output var
$mobile = "col-" . $datamobile . " ";
$tablet = "col-md-" . $datatablet . " ";
$desktop = "col-xl-" . $datadesktop . " ";
?>

<section class="single-portfolio">
	<div class="container">
		<div class="top-portfolio">
			<div class="row">
				<div class="col-xl-12">
					<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
				</div>
				<div class="col-xl-12 col-12">
					<div class="first-paragraph">
						<strong>
							<?php the_field( 'first_paragraph' ); ?>

						</strong>
					</div>
				</div>
				<div class="col-xl-8">
					<div class="short-description">
						<?php the_field( 'short_description' ); ?>
					</div>
				</div>
				<div class="col-xl-4">
					<div class="info-portfolio">
						<div class="inner-single-item">

							Client: <?php
							$base_name = "klient";
							$terms = wp_get_post_terms( $post->ID, $base_name);
							foreach ( $terms as $term ) {
								$srchome = "";
								echo '<a href="/' . $base_name . "/" . $term->slug . '">' . $term->name . '</a>';
							} ?>
						</div>
						<div class="inner-single-item">
							Type: <?php
							$base_name = "typ";
							$terms = wp_get_post_terms( $post->ID, $base_name);
							foreach ( $terms as $term ) {
								$srchome = "";
								echo '<a href="/' . $base_name . "/" . $term->slug . '">' . $term->name . '</a>';
							} ?>
						</div>
						<div class="inner-single-item">
							Date: <span><?php echo get_the_date(); ?></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content-portfolio mt-5">
			<div class="gallery">
				<div class="container">
					<div class="row">
						<?php 
						if( $images ): ?>
							<?php $time = 0; foreach( $images as $image ): ?>
							<figure class="<?php echo $mobile . $tablet . $desktop; ?>">
								<a class="wow fadeInUp js-smartPhoto" data-caption="<?php echo $image['description'] ?>" href="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>">
									<img style="height: <?php echo $height; ?>;" class="img-fluid w-100 item-gallery <?php echo $cropclass; ?>" alt="item-gallery" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" class="img-fluid">
								</a>
							</figure>
							<?php $time = $time +250; endforeach; ?>
						<?php endif;  ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-12">
			<div class="more-content mt-5">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="cpt">

		</div>
	</div>
</section>