<?php if ( have_rows( 'basic_service' )) : while ( have_rows( 'basic_service' )  ) : the_row(); ?>
	<section class="single-service">
		<div class="container">
			<div class="row">
				<div class="col-xl-6">
					<?php the_content(); ?>
				</div>
				<div class="col-xl-6">
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full-size' ); ?>
					<img class="img-fluid" src="<?php echo $url ?>" />
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; endif; ?>