<section class="service-6" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-md-6 col-12 wow fadeInLeft">
				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("tresc"); ?></p>
			</div>
			<div class="col-xl-6 col-md-6 col-12 wow fadeInRight">				
				<?php
				echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy, img-fluid") );
				?>
			</div>
		</div>
	</div>
</section> 