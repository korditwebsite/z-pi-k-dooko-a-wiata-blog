<section class="service-2" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-md-12 col-12 wow fadeInLeft">
				<div class="thumbnail responsive">
					<?php  echo wp_get_attachment_image( get_sub_field('grafika'), "kontener" ); ?>
				</div>
			</div>
			<div class="col-xl-12 col-md-12 col-12 wow fadeInRight">
				<div class="inner-text">
					<h2><?php the_sub_field("tytul"); ?></h2>
					<p><?php the_sub_field("tresc"); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>

