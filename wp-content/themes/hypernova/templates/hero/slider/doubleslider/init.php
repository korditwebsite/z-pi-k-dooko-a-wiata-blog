<?php if( have_rows('slider-content') ): ?>
    <div id="double-slider">
        <?php $licznik = 1; while ( have_rows('slider-content') ) : the_row(); 
        if ($licznik == 1) {
            $default = wp_get_attachment_image_url( get_sub_field('grafika'), "full", "", array( "class" => "theme-section-slider__image" ) );
        }
        $licznik = $licznik +1; endwhile; ?>
        <div class="slider" id="slider" style="--img-prev:url(/wp-content/uploads/2019/12/1.jpg);">
            <div class="slider__content" id="slider-content">
                <div class="slider__images">
                    <?php $x = 1; while ( have_rows('slider-content') ) : the_row();
                    if ($x == 1) {
                        $active = "slider__images-item--active";
                    } else {
                        $active = " ";
                    }
                    $grafika = wp_get_attachment_image( get_sub_field('grafika'), "full");
                    ?>

                    <div class="slider__images-item <?php echo $active; ?>" data-id="<?php echo $x; ?>">
                        <img src="<?php echo $grafika; ?>" >
                    </div>
                    <?php $x = $x + 1; endwhile; ?>
                </div>
                <div class="slider__text">
                    <?php $y = 1; while ( have_rows('slider-content') ) : the_row();
                    if ($y == 1) {
                        $active = "slider__text-item--active";
                    } else {
                        $active = " ";
                    }
                    ?>
                    <div class="slider__text-item <?php echo $active; ?>" data-id="<?php echo $y; ?>">
                        <div class="slider__text-item-head">
                            <h3><?php the_sub_field('naglowek'); ?></h3>
                        </div>
                        <div class="slider__text-item-info">
                            <p><?php the_sub_field('podpis'); ?></p>
                        </div>
                    </div>
                    <?php $y = $y + 1; endwhile; ?>
                </div>
            </div>
            <div class="slider__nav">
                <div class="slider__nav-arrows">
                    <div class="slider__nav-arrow slider__nav-arrow--left" id="left">to left</div>
                    <div class="slider__nav-arrow slider__nav-arrow--right" id="right">to right</div>
                </div>
                <div class="slider__nav-dots" id="slider-dots">
                    <?php $dot = 1; while ( have_rows('slider-content') ) : the_row();
                    if ($dot == 1) {
                        $active = "slider__nav-dot--active";
                    } else {
                        $active = " ";
                    }
                    ?>
                    <div class="slider__nav-dot" data-id="<?php echo $dot; ?>"></div>
                    <?php $dot = $dot + 1; endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php  else : endif; ?>