<section id="slick">
  <?php if( have_rows('slider-content') ): ?>
    <div class="split-slideshow">
      <div class="slideshow">
        <div class="slider">
         <?php while ( have_rows('slider-content') ) : the_row();
          ?>
          <div class="item">
            <?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array() );  ?>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
    <div class="slideshow-text">
      <?php while ( have_rows('slider-content') ) : the_row(); ?>
        <div class="item item-text">
          <?php the_sub_field('naglowek'); ?>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
  <?php  else : endif; ?>
  <?php if ( have_rows( 'przycisk_slick' ) ) :  while ( have_rows( 'przycisk_slick' ) ) : the_row(); ?>
  <div class="slick-scroll">
    <a href="<?php the_sub_field( 'link' ); ?>"><?php the_sub_field( 'napis' ); ?></a>
  </div>
  <?php endwhile; endif; ?>
</section>