<?php
//inicjacja ustawień
$hn_url_templates = get_template_directory_uri() . "/templates/hero/";
if( get_row_layout() == 'slider' ): 
	include('wp-content/themes/hypernova/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	include('wp-content/themes/' . wp_get_theme() . '/templates/hero/slider/init.php');


elseif( get_row_layout() == 'hero_image' ): 
	include('/wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('/wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		add_css_theme("logotype-1", "/templates/hero/logotype/scss/style.css");
		include('/wp-content/themes/' . wp_get_theme() . '/templates/hero/logotype/init.php');
	endwhile; endif;
elseif( get_row_layout() == 'wybierz_jedno' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	include('wp-content/themes/' . wp_get_theme() . '/templates/hero/choose/init.php');
endif;
?>
