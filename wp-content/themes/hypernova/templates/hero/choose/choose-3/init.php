<section class="choose">
	<div class="<?php echo $hn_size_container; ?>">
		<div class="choose-inner-html">
			<?php if ( have_rows( 'wiecej_niz_1' ) ) : while ( have_rows( 'wiecej_niz_1' ) ) : the_row(); ?>
				<div class="single-item-choose">
					<div class="choose-bg">
						<div class="content">
							<h3><?php the_sub_field( 'title' ); ?></h3>
							<h5><?php the_sub_field( 'subtitle' ); ?></h5>
							<?php if ( have_rows( 'button' ) ) : while ( have_rows( 'button' ) ) : the_row(); ?>
								<a href="<?php the_sub_field( 'link' ); ?>">
									<button><?php the_sub_field( 'text' ); ?></button>
								</a>
							<?php endwhile; endif; ?>
						</div> 
						<div class="bg-image">
							<?php echo wp_get_attachment_image( get_sub_field('image'), "full" ); ?>
						</div>
					</div>
				</div>
			<?php endwhile; endif; ?> 
		</div>
	</div>
</section>