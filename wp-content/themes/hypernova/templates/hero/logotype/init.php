<section class="presentation-logotype" style="background-color: <?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity:<?php echo $hn_opacity; ?>;"></div>
	<?php if (get_sub_field( 'video' )): ?>
		<div class="checked-video">
			<video autoplay src="<?php the_sub_field( 'video' ); ?>"></video>
		</div>
	<?php endif ?>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="content-logotype">
			<div class="max-size <?php echo $hn_rwd; ?>">
				<?php if (get_sub_field( 'logotyp' )): ?>
					<div class="checked-logo">
						<?php echo id_subimage("logotyp", "full") ?>
					</div>
				<?php endif ?>
				<div class="checked-default">
					<h3 style="color:<?php echo $hn_fcolor; ?>"><?php the_sub_field( 'tytul' ); ?></h3>
				</div>
				<?php if (get_sub_field( 'podpis' )): ?>
					<div style="color:<?php echo $hn_scolor; ?>" class="checked-text">
						<?php the_sub_field( 'podpis' ); ?>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</section>


