<?php 
$background = wp_get_attachment_image( get_sub_field('background'), "full" );
$bgcolor = get_sub_field( 'background_color' );
$opacity = get_sub_field( 'opacity' );
$text = get_sub_field( 'main_text' );
$logo = wp_get_attachment_image( get_sub_field('logo'), "full" );
$vachromatic = get_sub_field( 'monochrome' );
$chromatic = "color-" . strtolower($vachromatic);
?>
<section class="presentation-logotype">
	<div class="full-screen-present" style="background-color: <?php echo $bgcolor; ?>;">
		<div class="bg-present">
			<div class="bg-present-image" style="opacity: <?php echo $opacity; ?>">
				<?php echo $background; ?>				
			</div>
			<div class="inner-content">
				<div class="logo-image <?php echo $chromatic; ?> wow fadeInDown">
					<?php echo $logo; ?>
				</div>
				<h3 data-wow-delay="1s" class="<?php echo $chromatic; ?> wow fadeInDown"><?php echo $text; ?></h3>
			</div>
		</div>
	</div>	
</section>


