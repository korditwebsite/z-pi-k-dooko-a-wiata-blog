<section class="paralax" id="<?php the_sub_field("id_sekcji"); ?>" style="background: <?php echo $hn_bgcolor ?>">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground ?>); opacity: <?php echo $hn_opacity; ?>"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<?php if ($hn_ulozenie == 1): ?>
			<div class="row">
				<?php if ( have_rows( 'tekstowy' ) ) : while ( have_rows( 'tekstowy' ) ) : the_row(); ?>

					<div class="text-box">
						<h3 class="text-center" style="color: <?php echo $hn_fcolor ?>"><?php the_sub_field( 'tytul' ); ?></h3>
						<div class="text text-center" style="color: <?php echo $hn_scolor; ?>">
							<?php the_sub_field( 'opis' ); ?>
						</div>
					</div>
				<?php endwhile; endif; ?>
			</div>
		<?php endif ?>
		<?php if ($hn_ulozenie == 2): ?>
			<div class="row">
				<h3 class="text-center" style="color: <?php echo $hn_fcolor ?>"><?php the_sub_field( 'tytul' ); ?></h3>
				<?php if ( have_rows( 'liczniki' ) ) : while ( have_rows( 'liczniki' ) ) : the_row(); ?>	<div class="<?php echo $hn_rwd ?>">
					<div class="number-box--single text-center">
						<div class="thumbnail" style="color: <?php echo $hn_fcolor ?>"><?php echo id_subimage('grafika', 'about-small-1'); ?></div>
						<div class="title" style="color: <?php echo $hn_fcolor ?>"><?php the_sub_field( 'tytul' ); ?></div>
						<div class="count-number" style="color: <?php echo $hn_scolor ?>">+<span><?php the_sub_field( 'liczba' ); ?></span></div>
					</div>
				</div>	
			<?php endwhile; endif; ?>
		</div>
	<?php endif ?>

</div>
</div>
</div>
</section>  