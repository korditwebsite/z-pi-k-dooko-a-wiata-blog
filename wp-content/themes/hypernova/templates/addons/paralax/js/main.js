  var wow = new WOW(
  {
    boxClass:     'paralax',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      var counters = $(".count-number span");
      var countersQuantity = counters.length;
      var counter = [];

      for (i = 0; i < countersQuantity; i++) {
        counter[i] = parseInt(counters[i].innerHTML);
      }

      var count = function(start, value, id) {
        var localStart = start;
        setInterval(function() {
          if (localStart < value) {
            localStart++;
            counters[id].innerHTML = localStart;
          }
        }, 40);
      }

      for (j = 0; j < countersQuantity; j++) {
        count(0, counter[j], j);
      }
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
  );
  wow.init(); 
  new WOW().init();