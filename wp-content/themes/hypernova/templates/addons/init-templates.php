<?php
//inicjacja ustawień

//LIST
if( get_row_layout() == 'list' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		include('wp-content/themes/' . wp_get_theme() . '/templates/addons/list/init.php');
	endwhile; endif;


//ZESPÓŁ
elseif( get_row_layout() == 'team' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		add_css_theme("choose-1", "/templates/addons/employe/scss/style.css");
		include('wp-content/themes/' . wp_get_theme() . '/templates/addons/employe/init.php');

	endwhile; endif;

elseif( get_row_layout() == 'collapse' ): 
	include('wp-content/themes/' . wp_get_theme() . 'templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . 'templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		add_css_theme("choose-1", "/templates/addons/collapse/scss/style.css");
		include('wp-content/themes/' . wp_get_theme() . 'templates/addons/collapse/init.php');

	endwhile; endif;


elseif( get_row_layout() == 'carousel_logotype' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	add_css_theme("logo-carousel-1", "/templates/addons/logo-carousel/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		include('wp-content/themes/' . wp_get_theme() . '/templates/addons/logo-carousel/init.php');

	endwhile; endif;

elseif( get_row_layout() == 'paralax' ): 
	include('wp-content/themes/' . wp_get_theme() . 'templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . 'templates/style-container.php');
//inicjacja template
	add_css_theme("logo-carousel-1", "/templates/addons/paralax/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		wp_enqueue_script( 'counter-number', get_template_directory_uri() . 'templates/addons//paralax/js/main.js', array());
		include('wp-content/themes/' . wp_get_theme() . 'templates/addons/paralax/init.php');
	endwhile; endif;


//GALERIA
elseif( get_row_layout() == 'gallery' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		add_css_theme("gallery-1", "/templates/addons/gallery/scss/style.css");
		wp_enqueue_script( 'gallery', get_template_directory_uri() . '/assets/includes/general/ecogallery/gallery.js', array());
		wp_enqueue_script( 'init-gallery', get_template_directory_uri() . '/assets/includes/general/ecogallery/init.js', array());
		wp_enqueue_style( 'gallery-css', get_template_directory_uri() . '/assets/includes/general/ecogallery/gallery.css' );
		include('wp-content/themes/' . wp_get_theme() . '/templates/addons/gallery/init.php');

	endwhile; endif;

endif;
?>
