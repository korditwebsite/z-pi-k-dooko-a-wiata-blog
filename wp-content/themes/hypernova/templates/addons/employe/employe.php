<?php 
//Size container
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
    $datamobile = get_sub_field( 'mobile' ); 
    $datadesktop = get_sub_field( 'desktop' ); 
endwhile; endif;
$title = get_sub_field( 'tytul' );
$themelink = get_template_directory_uri();
//output var
$mobile = "col-" . $datamobile . " ";
$desktop = "col-xl-" . $datadesktop . " ";

?>
<section class="employees">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><?php echo $title; ?></h2>
            </div>
        </div>
        <div class="row">
            <?php if( have_rows('team_box') ): while( have_rows('team_box') ): the_row(); ?>
                <div class="<?php echo $mobile . $desktop; ?>">
                    <div class="employees-wrapper">
                        <?php echo id_subimage("images","big"); ?>
                        <div class="employees-box">
                            <h4><?php the_sub_field("name"); ?></h4>
                            <p class="position"><?php the_sub_field("position"); ?></p>
                            <a href="tel:+48:<?php the_sub_field("phone"); ?>">
                                <?php exportsvg("/assets/img/phone.svg"); ?>
                                <?php the_sub_field("phone"); ?>
                            </a>
                            <a href="mailto:<?php the_sub_field("mail"); ?>">
                                <?php 
                                exportsvg("/assets/img/mail.svg");
                                ?>
                                <?php the_sub_field("mail"); ?></a>
                            </div>
                            <div class="text-employees">
                                <?php the_sub_field("info"); ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </section>

    