<section class="collapse-section" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("tresc"); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 col-md-6 col-12">
				<div id="accordion">
					<div class="card">
						<div class="card-header" id="headingOne">
							<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Piach podsypkowy stosowany do:
							</button>
							</h5>
						</div>
						<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body">
								<ul>
									<li>Nasypów drogowych</li>
									<li>Uzupełniania ubytków terenowych</li>
									<li>Zasypywania fundamentów</li>
									<li>Zasypywania i zagęszczenia wykopów pod sieci uzbrojenia terenu</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingTwo">
							<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							Piach siany stosowany do:
							</button>
							</h5>
						</div>
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							<div class="card-body">
								<ul>
									<li>Murowania</li>
									<li>Drenaży</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingThree">
							<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							Piach płukany i wiślany stosowany do:
							</button>
							</h5>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							<div class="card-body">
								<ul>
									<li>Wylewek</li>
									<li>Murowanie</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="headingFour">
							<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							Piach budowlany stosowany do:
							</button>
							</h5>
						</div>
						<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
							<div class="card-body">
								<ul>
									<li>Betonu</li>
									<li>Zapraw</li>
									<li>Stabilizacji cementem</li>
									<li>Mas bitumicznych</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-md-6 col-12">
				<?php echo wp_get_attachment_image( get_sub_field('grafika'), "o-nas", "", array( "class" => "js-smartPhoto img-fluid", "alt" => "rzut" ) );  ?>
			</div>
		</div>
	</div>
</section>