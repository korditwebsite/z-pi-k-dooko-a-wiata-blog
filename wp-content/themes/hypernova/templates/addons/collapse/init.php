
<section class="collapse-section" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<?php if ( have_rows( 'faq' ) ) : while ( have_rows( 'faq' ) ) : the_row(); ?>
			<div class="row">
				<div class="col-lg-12">
					<h2><?php the_sub_field("title"); ?></h2>
					<p><?php the_sub_field("subtitle"); ?></p>
				</div>
			</div>
			<div class="row">
				<div class="<?php echo $hn_rwd; ?>">

					<div id="faq">
						<?php if ( have_rows( 'questions' ) ) : $x = 0; while ( have_rows( 'questions' ) ) : the_row();
							if ($x == 0) {
								$active = 'show';
							} else {
								$active = ' ';
							}
							?>
							<div class="card">
								<div class="card-header" id="heading-<?php echo $x; ?>" data-toggle="collapse" data-target="#collapse-<?php echo $x; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $x; ?>">
									<?php the_sub_field( 'pytanie' ); ?>
								</div>
								<div id="collapse-<?php echo $x; ?>" class="collapse <?php echo $active; ?>" aria-labelledby="heading-<?php echo $x; ?>" data-parent="#faq">
									<div class="card-body">
										<?php the_sub_field( 'odpowiedz' ); ?>
									</div>
								</div> 
							</div>
							<?php $x = $x + 1; endwhile;  endif; ?>
						</div>
					</div>
				</div>
			<?php endwhile;  endif; ?>
		</div>
	</section>

