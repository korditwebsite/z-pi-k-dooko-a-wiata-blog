<section class="list-section" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="title-container text-center">
			<h3 style="color: <?php echo $hn_fcolor; ?>;"><?php the_sub_field("title"); ?></h3>
			<p style="color: <?php echo $hn_fcolor; ?>;"><?php the_sub_field("subtitle"); ?></p>
		</div>
		<ul class="row">
			<?php if ( have_rows( 'lista' ) ) : while ( have_rows( 'lista' ) ) : the_row(); ?>
				<li class="col-12" style="color:<?php echo $hn_scolor; ?>">
					<div class="row">
						<div class="col-xl-8"><?php the_sub_field( 'etykieta' ); ?></div>
						<div class="col-xl-2">
							<?php if (get_sub_field( 'pdf' )) : ?>
								<a href="<?php the_sub_field( 'pdf' ); ?>" download>
									<button>pobierz pdf</button>
								</a>
							<?php endif ?>
						</div>
						<div class="col-xl-2">
							<?php if (get_sub_field( 'docx' )) : ?>
								<a href="<?php the_sub_field( 'docx' ); ?>" download>
									<button>pobierz docx</button>
								</a>
							<?php endif ?>
						</div>
					</div>
				</li>
			<?php endwhile; endif; ?>
		</ul>
	</div>
</section>


