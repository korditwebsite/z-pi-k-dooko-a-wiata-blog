<?php
//Jeśli nie ustawia się w linii, należy zmienić $var_number na minus lub plus
$var_number = 2;
$dataall = $hn_desktop + $var_number; 
$enddataall = $dataall - 1; ?>
<section class="section-logo-carousel" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="row">
			<h2 class="text-center w-100"><?php the_sub_field( 'tytul' ); ?></h2>
			<p class="text-center w-100 mb-5"><?php the_sub_field( 'tresc' ); ?></p>
		</div>
		<div class="row">
			<div id="carousel-logo" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php if( have_rows('Logo') ): $x = 0; $time = 0; while ( have_rows('Logo') ) : the_row();
						if ($x == 0) {
							$active = "active";
						} else {
							$active = " ";
						}
						?>
						<?php if (($x % $dataall) == 0 || $x == 0) : ?>

							<div class="carousel-item <?php echo $active; ?>">
								<div class="row">
								<?php endif ?>
								<div class="<?php echo $hn_rwd; ?>">
									<a href="<?php the_sub_field("logo-link") ?>" target="_blank">
										<?php echo id_subimage("grafika", "small-logo"); ?>
									</a>
								</div>
								<?php if (($x % $dataall) == $enddataall) : ?>
								</div>
							</div>

						<?php endif ?>
						<?php $x = $x +1;
					endwhile; else : endif; ?>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carousel-logo" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carousel-logo" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>
</section>