<?php
//inicjacja ustawień
if( get_row_layout() == 'wyswietl_cpt' ): 
	include('/wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('/wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if( have_rows('informacje_o_sekcji') ): while ( have_rows('informacje_o_sekcji') ) : the_row();
		include('/wp-content/themes/' . wp_get_theme() . '/templates/cpt/cpt.php');
	endwhile; endif;
endif;
?>
