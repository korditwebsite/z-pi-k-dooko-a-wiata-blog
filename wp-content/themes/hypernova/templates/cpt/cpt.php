<?php 
$wybor = $hn_ulozenie;
$postchoose = get_sub_field( 'wybierz_posty' );
$categorychoose = get_sub_field( 'wybrane_kategorie' );
$categorysingle = get_sub_field( 'dana_kategoria' );
$namefield = get_sub_field( 'category_name' );
$opacity = $hn_opacity;
$img =  $hn_imagebackground;
$color = $hn_bgcolor;
?>

<section id="blog-page" class="<?php the_sub_field( 'id_kategorii' ); ?>">
	<div class="paralax-image" style=" background-color: <?php echo $color; ?>">
		<div class="img-paralax" style="background-image: url(<?php echo $img; ?>);opacity: <?php echo $opacity ?>;">

		</div>
	</div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="row">
			<div class="col-xl-12">
				<div class="title-container mt-4">
					<h3 class="text-center w-100"><?php the_sub_field( 'title' ); ?></h3>
					<p class="text-center w-100"><?php the_sub_field( 'subtitle' ); ?></p>
				</div>
			</div>
		</div>
		<?php if ($wybor == 2): ?>
			<div class="row">
				<?php
				$licznikforeach = 0;

				foreach ($categorychoose as $categorychoosen) :
					$args = array(
						'post_type'   => $namefield,
						'post_status' => 'publish',
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => $categorychoose[$licznikforeach]->taxonomy,
								'field' => 'term_id',
								'terms' => $categorychoose[$licznikforeach]->term_id
							)
						)
					);
					$link = "/" . $categorychoose[$licznikforeach]->taxonomy . "/" . $categorychoose[$licznikforeach]->slug;
					?>
					<div class="<?php echo $hn_rwd ?>">
						<div class="inner-post-list">
							<div class="thumbnail">
								<?php 
								$term = get_queried_object();
								$image = get_field('grafika_kategorii', 'category_'.$categorychoosen->term_id);    
								?>
								<img class="img-fluid mb-3" src="<?php echo $image; ?>">
							</div>
							<div class="inner-content">
								<div class="link">
									<a href="<?php echo $link; ?>">
										<?php echo $categorychoose[$licznikforeach]->name; ?>
									</a>
								</div>
							</div>
						</div>
					</div>
					<?php $licznikforeach = $licznikforeach +1; endforeach ?>
				</div>
			<?php endif ?>
			<?php if ($wybor == 1): ?>
				<div class="row">
					<?php 
					$posts = $postchoose;
					if( $posts ): ?>
						<?php foreach( $posts as $p ):
							?>
							<div class="<?php echo $hn_rwd; ?>">
								<div class="inner-post-list">
									<div class="thumbnail">
										<?php $featured_img_url = get_the_post_thumbnail_url($p->ID, 'full');
										?>
										<img class="mb-3" src="<?php echo $featured_img_url; ?>">
									</div>
									<div class="inner-content">
										<div class="link">
											<a href="<?php echo get_permalink( $p->ID ); ?>">
												<?php echo get_the_title( $p->ID ); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			<?php endif ?>
			<?php if ($wybor == 3): ?>
				<div class="row">
					<?php
					$case_study_cat_slug = get_queried_object()->slug;
					?>
					<?php
					$al_tax_post_args = array(
						'post_type' => $namefield,
						'posts_per_page' => 999,
						'order' => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => 'typ',
								'field' => 'term_id',
								'terms' => 4
							)
						)
					);
					$al_tax_post_qry = new WP_Query($al_tax_post_args);
					if($al_tax_post_qry->have_posts()) :
						while($al_tax_post_qry->have_posts()) :
							$al_tax_post_qry->the_post();
							?>
							<div class="<?php echo $hn_rwd ?>">
								<div class="inner-post-list">
									<div class="thumbnail">
										<?php the_post_thumbnail(); ?>
									</div>
									<div class="inner-content">
										<div class="link">
											<a href="<?php echo get_permalink(); ?>">
												<?php echo get_the_title(); ?>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php
						endwhile;
					endif;
					?>
				</div>
			<?php endif ?>
		</div>
	</section>
