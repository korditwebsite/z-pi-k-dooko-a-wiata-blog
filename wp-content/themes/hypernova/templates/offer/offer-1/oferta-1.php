<section class="oferta-1" id="<?php the_sub_field("id_sekcji"); ?>" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "hero_image" ); ?>);">
	<div class="container">
		<div class="row">
			<h2>Katolog <span class="red">profili</span></h2>
		</div>
		<div class="row">
			<?php if( have_rows('oferta_pojedyncza') ):  while ( have_rows('oferta_pojedyncza') ) : the_row();  ?>
			<div class="col-md-6 col-12 wow zoomIn">
				<div class="offer-wrapper">
					<div class="offer-box">
						<div class="offer-box-wrapper">
							<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy, img-fluid", "data-src=" => $grafika ) );  ?>
							<h3><?php the_sub_field("tytul"); ?></h3>
							<p><?php the_sub_field("tresc"); ?></p>
						</div>
						<a href="<?php the_sub_field("pdf"); ?>" target="_blank">Pobierz plik</a>
					</div>
				</div>
			</div>
			<?php endwhile; else : endif; ?>
		</div>
	</div>
</section>