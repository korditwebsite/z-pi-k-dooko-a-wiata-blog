<?php
$hn_desktop = " ";
$hn_tablet = " ";
$hn_mobile = " ";
$hn_ulozenie = " ";
if ( have_rows( 'ustawienia' ) ) : while ( have_rows( 'ustawienia' ) ) : the_row();
	$hn_desktop = get_sub_field( 'desktop' );
	$hn_tablet = get_sub_field( 'Tablet' );
	$hn_mobile = get_sub_field( 'Mobile' );
	$hn_ulozenie = get_sub_field( 'ulozenie' );
	$hn_rwd = "col-xl-" . $hn_desktop . " col-md-" . $hn_tablet . " col-" . $hn_mobile;

//size container 
	$hn_size = get_sub_field( 'rozmiar_kontenera' );
	if ($hn_size == 1) {
		$hn_size_container = "container-fluid";
	}
	elseif ($hn_size == 2) {
		$hn_size_container = "container";
	}
	elseif ($hn_size == 3) {
		$hn_size_container = get_sub_field( 'nazwa_klasy' );
	}
endwhile; endif;
$hn_rwd = "col-xl-" . $hn_desktop . " col-md-" . $hn_tablet . " col-" . $hn_mobile;
?> 