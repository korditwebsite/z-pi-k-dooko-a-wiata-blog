<?php
if ( have_rows( 'header-settings', 'option' ) ) : 
	while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
		$nbrBtn = get_sub_field( 'button_navi' );
		$wyborheadera = get_sub_field( 'header_sekcja' ); 
	endwhile;
endif;

add_css_theme("header-" . $wyborheadera, "/templates/header/header-" . $wyborheadera . "/scss/style.css");
get_template_part( 'templates/header/header-' . $wyborheadera . '/header', $wyborheadera );

get_template_part( '/templates/header/hamburger-menu/init'); 
