
<?php 
$terms = get_sub_field('taksonomia');
if( $terms ): ?>
  <section class="theme-section-slider">
    <div id="postslider" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="container">
        <div class="position-ver">
          <h4>Najnowsze wpisy:</h4>
          <ol class="carousel-indicators">
           <?php $y = 0; foreach( $terms as $term ):
           if ($y == 0) {
            $active = "active";
          } else {
            $active = " ";
          }
          ?>
          <li data-target="#postslider" data-slide-to="<?php echo $y; ?>" class="<?php echo $active; ?>">
            <img src="<?php echo get_the_post_thumbnail_url(esc_html( $term->ID ), 'full-size'); ?>" alt="First slide">
            <h5><?php echo esc_html( $term->post_title ); ?></h5>
          </li>
          <?php $y = $y + 1; endforeach; ?>
        </ol>
      </div>
    </div>
    <div class="carousel-inner">
      <?php $x = 0; foreach( $terms as $term ):
      if ($x == 0) {
        $active = "active";
      } else {
        $active = " ";
      }
      ?>
      <div class="carousel-item <?php echo $active; ?>">

        <img class="d-block w-100" src="<?php echo get_the_post_thumbnail_url(esc_html( $term->ID ), 'full-size'); ?>" alt="First slide">
        <div class="container">
          <div class="carousel-opis">
            <div class="row">
              <div class="col-12 col-md-7 col-xl-7 rozdzielczosc">
                <div class="slider-single-item">
                  <h3><?php echo esc_html( $term->post_title ); ?></h3>
                  <p><?php echo esc_html( $term->post_excerpt ); ?> </p>
                  <data><?php echo get_the_date( 'Y-m-d' ); ?></data>
                  <div class="categories-container">
                    <b>kategoria:</b>
                    <?php echo get_the_category_list( '| ', ',', $term->ID ); ?>
                  </div>
                  <?php 
                  $tags = get_tags();
                  $html = '<div class="post_tags">TAGI: ';
                  foreach ( $tags as $tag ) {
                    $tag_link = get_tag_link( '| ', $tag->term_id );

                    $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
                    $html .= "{$tag->name}, </a>";
                  }
                  $html .= '</div>';
                  echo '<div class="single-tag">'. $html . " </div>";
                  ?>
                  <div class="container-href">
                    <a href="<?php echo get_permalink($term); ?>">czytaj więcej</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php $x = $x + 1; endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#postslider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#postslider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <div class="gradient-slider"></div>
</section>
<?php endif; ?>
<div class="all-content">
  <div class="container">
    <div class="row">
      <div class="col-xl-8">
        <div class="all-section-wrapper">
          <div class="all-section">
            <section class="latest-post">
              <h2>Najnowsze posty</h2>
              <?php
              $args = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'tax_query' => array(
                  array(
                    'taxonomy' => 'category',
                    'field'    => 'id',
                    'terms'    => 1,
                  ),
                ),
              );

              $post_query = new WP_Query($args);
              if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                  $post_query->the_post();
                  ?>
                  <div class="inner-post-list">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumbnail">
                          <?php the_post_thumbnail( 'category-thumb' ); ?>
                        </div>
                      </div>
                      <div class="col-xl-7">
                        <div class="inner-content">
                          <div class="row">
                            <div class="col-9">
                              <div class="categories">
                                <?php echo get_the_category_list( ' ', ',', $term->ID ); ?>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="data">
                                <b>
                                  <?php echo get_the_date( 'Y' ); ?>
                                </b>
                                <?php echo get_the_date( 'm-d' ); ?>
                              </span>
                            </div>
                          </div>
                          <h3>
                            <?php the_title(); ?>
                          </h3>

                          <div class="excerpt">
                            <?php 
                            if (has_excerpt()) {
                              the_excerpt();
                            } else {
                              echo wp_trim_words( get_the_excerpt(), 20 );
                            }

                            ?>
                          </div>
                          <div class="link">
                            <a href="<?php echo get_permalink($parent_id); ?>">
                              <?php _e( 'Czytaj więcej', 'textdomain' ); ?>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
              <a class="button-special" href="/2019">Sprawdź archiwum</a>
            </section>
            <section class="latest-post">
              <h2>Top posty</h2>
              <?php
              $args = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'tax_query' => array(
                  array(
                    'taxonomy' => 'category',
                    'field'    => 'id',
                    'terms'    => 10,
                  ),
                ),

              );

              $post_query = new WP_Query($args);
              if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                  $post_query->the_post();
                  ?>
                  <div class="inner-post-list">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumbnail">
                          <?php the_post_thumbnail( 'category-thumb' ); ?>
                        </div>
                      </div>
                      <div class="col-xl-7">
                        <div class="inner-content">
                          <div class="row">
                            <div class="col-9">
                              <div class="categories">
                                <?php echo get_the_category_list( ' ', ',', $term->ID ); ?>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="data">
                                <b>
                                  <?php echo get_the_date( 'Y' ); ?>
                                </b>
                                <?php echo get_the_date( 'm-d' ); ?>
                              </span>
                            </div>
                          </div>
                          <h3>
                            <?php the_title(); ?>
                          </h3>

                          <div class="excerpt">
                            <?php 
                            if (has_excerpt()) {
                              the_excerpt();
                            } else {
                              echo wp_trim_words( get_the_excerpt(), 20 );
                            }

                            ?>
                          </div>
                          <div class="link">
                            <a href="<?php echo get_permalink($parent_id); ?>">
                              <?php _e( 'Czytaj więcej', 'textdomain' ); ?>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
              <a class="button-special" href="/2019">Sprawdź top posty</a>
            </section>
            <section class="latest-post">
              <h2>Analizy</h2>
              <?php
              $args = array(
                'post_type' => 'post',
                'posts_per_page' => 5,
                'tax_query' => array(
                  array(
                    'taxonomy' => 'category',
                    'field'    => 'id',
                    'terms'    => 9,
                  ),
                ),
              );

              $post_query = new WP_Query($args);
              if($post_query->have_posts() ) {
                while($post_query->have_posts() ) {
                  $post_query->the_post();
                  ?>
                  <div class="inner-post-list">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumbnail">
                          <?php the_post_thumbnail( 'category-thumb' ); ?>
                        </div>
                      </div>
                      <div class="col-xl-7">
                        <div class="inner-content">
                          <div class="row">
                            <div class="col-9">
                              <div class="categories">
                                <?php echo get_the_category_list( ' ', ',', $term->ID ); ?>
                              </div>
                            </div>
                            <div class="col-3">
                              <span class="data">
                                <b>
                                  <?php echo get_the_date( 'Y' ); ?>
                                </b>
                                <?php echo get_the_date( 'm-d' ); ?>
                              </span>
                            </div>
                          </div>
                          <h3>
                            <?php the_title(); ?>
                          </h3>

                          <div class="excerpt">
                            <?php 
                            if (has_excerpt()) {
                              the_excerpt();
                            } else {
                              echo wp_trim_words( get_the_excerpt(), 20 );
                            }

                            ?>
                          </div>
                          <div class="link">
                            <a href="<?php echo get_permalink($parent_id); ?>">
                              <?php _e( 'Czytaj więcej', 'textdomain' ); ?>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
              <a class="button-special" href="/2019">Sprawdź wszystkie analizy</a>
            </section>
          </div>
        </div>
      </div>
      <div class="col-xl-4">
        <aside>
          <?php echo get_sidebar(); ?>
        </aside>
      </div>
    </div>
  </div>
</div>
