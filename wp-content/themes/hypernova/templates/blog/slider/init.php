<?php 
if ($hn_ulozenie == 1) {
	add_css_theme("slider-blog-1", "/templates/blog/slider/bootstrap/scss/style.css");
	get_template_part( '/templates/blog/slider/bootstrap/init'); 
}
elseif ($hn_ulozenie == 2) {
	add_css_theme("slick-1", "/templates/hero/slider/slick/scss/style.css");
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		get_template_part( '/templates/hero/slider/slick/slick'); 
	endwhile; endif;
	wp_enqueue_script( 'jquery-mouse', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.12/jquery.mousewheel.js');
	wp_enqueue_script( 'slick-slider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js');
	wp_enqueue_script( 'slick-slider', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js');
	wp_enqueue_script( 'slick-slider-inner-js', '/wp-content/themes/' . wp_get_theme() . '/templates/hero/slider/slick/js/main.js');
}
elseif ($hn_ulozenie == 3) {
	add_css_theme("circle-1", "/templates/hero/slider/circle/scss/style.css");
	wp_enqueue_script( 'slick-slider-inner-js', '/wp-content/themes/' . wp_get_theme() . '/templates/hero/slider/circle/js/main.js');
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		get_template_part( '/templates/hero/slider/circle/init'); 
	endwhile; endif;
}
elseif ($hn_ulozenie == 4) {
	add_css_theme("double-1", "/templates/hero/slider/doubleslider/scss/style.css");
	wp_enqueue_script( 'slick-slider-inner-js', '/wp-content/themes/' . wp_get_theme() . '/templates/hero/slider/doubleslider/js/main.js');
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();
		get_template_part( '/templates/hero/slider/doubleslider/init'); 
	endwhile; endif;
}
