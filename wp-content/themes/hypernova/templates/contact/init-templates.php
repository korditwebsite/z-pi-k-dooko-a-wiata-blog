<?php
//inicjacja ustawień
if( get_row_layout() == 'podstawowy_kontakt' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row(); 
		add_css_theme("contact-style-" . "1", "/templates/contact/contact-1/scss/style.css");
		include('wp-content/themes/' . wp_get_theme() . '/templates/contact/contact-1/contact.php');
	endwhile; endif;



elseif( get_row_layout() == 'rozbudowany_kontakt' ): 
	include('wp-content/themes/' . wp_get_theme() . '/templates/settings-container.php');
//inicjacja styli
	include('wp-content/themes/' . wp_get_theme() . '/templates/style-container.php');
//inicjacja template
	if ( have_rows( 'informacje_o_sekcji' ) ) : while ( have_rows( 'informacje_o_sekcji' ) ) : the_row();

		include('wp-content/themes/' . wp_get_theme() . '/templates/contact/contact-2/contact.php');
	endwhile; endif;
endif;
?>
