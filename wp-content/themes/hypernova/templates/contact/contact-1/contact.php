<?php 
$colortext = get_sub_field( 'color_text' );
$colorbg = get_sub_field( 'background_color' );
$opacity = get_sub_field( 'opacity_background' );
$svgcolor = get_sub_field( 'color_text' );
$imagebg = id_subimage_url("tlo", "hero_image");
$versioncolor = "color-" . get_sub_field( 'versioncolor' ); 
?>
<section class="kontakt kontakt-1" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="<?php echo $hn_size_container; ?>">
		<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
	</div>
	<div class="container">
		<div class="row wowparalax">
			<div class="col-xl-6">
				<div class="inner-opis">
					<h2 style="color:<?php echo $colortext; ?>;"><?php the_sub_field("tytul"); ?></h2>
					<div class="phone wow">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						viewBox="0 0 480.56 480.56" xml:space="preserve">
						<g>
							<g>
								<path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
								c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
								c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
								c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
								c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
								c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
								<path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
								c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
								<path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
								l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
							</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
							<div class="all-numbers">
								<?php if( have_rows('numery_telefonow') ): while ( have_rows('numery_telefonow') ) : the_row(); ?>
									<div class="box-mobile-contact">
										<a style="color:<?php echo $colortext; ?>;" href="<?php the_sub_field('numer_tel'); ?>">
											<?php the_sub_field('numer_tel'); ?></a>
										</div>
									<?php endwhile; else : endif; ?>
								</div>
							</div>
							<div class="mail wow">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512px" height="512px">
									<g> 
										<g>
											<g>
												<path d="M486.4,59.733H25.6c-14.138,0-25.6,11.461-25.6,25.6v341.333c0,14.138,11.461,25.6,25.6,25.6h460.8     c14.138,0,25.6-11.461,25.6-25.6V85.333C512,71.195,500.539,59.733,486.4,59.733z M494.933,426.667     c0,4.713-3.82,8.533-8.533,8.533H25.6c-4.713,0-8.533-3.82-8.533-8.533V85.333c0-4.713,3.82-8.533,8.533-8.533h460.8     c4.713,0,8.533,3.82,8.533,8.533V426.667z" fill="#FFFFFF"/>
												<path d="M470.076,93.898c-2.255-0.197-4.496,0.51-6.229,1.966L266.982,261.239c-6.349,5.337-15.616,5.337-21.965,0L48.154,95.863     c-2.335-1.96-5.539-2.526-8.404-1.484c-2.865,1.042-4.957,3.534-5.487,6.537s0.582,6.06,2.917,8.02l196.864,165.367     c12.688,10.683,31.224,10.683,43.913,0L474.82,108.937c1.734-1.455,2.818-3.539,3.015-5.794c0.197-2.255-0.51-4.496-1.966-6.229     C474.415,95.179,472.331,94.095,470.076,93.898z" fill="#FFFFFF"/>
												<path d="M164.124,273.13c-3.021-0.674-6.169,0.34-8.229,2.65l-119.467,128c-2.162,2.214-2.956,5.426-2.074,8.392     c0.882,2.967,3.301,5.223,6.321,5.897c3.021,0.674,6.169-0.34,8.229-2.65l119.467-128c2.162-2.214,2.956-5.426,2.074-8.392     C169.563,276.061,167.145,273.804,164.124,273.13z" fill="#FFFFFF"/>
												<path d="M356.105,275.78c-2.059-2.31-5.208-3.324-8.229-2.65c-3.021,0.674-5.439,2.931-6.321,5.897     c-0.882,2.967-0.088,6.178,2.074,8.392l119.467,128c3.24,3.318,8.536,3.442,11.927,0.278c3.391-3.164,3.635-8.456,0.549-11.918     L356.105,275.78z" fill="#FFFFFF"/>
											</g>
										</g>
									</g>
								</svg>
								<div class="box-mobile-contact">
									<?php if( have_rows('adres_e-mail') ): while ( have_rows('adres_e-mail') ) : the_row(); ?>
										<a style="color:<?php echo $colortext; ?>;" href="<?php the_sub_field('mail'); ?>"><?php the_sub_field('mail'); ?></a>
									<?php endwhile; else : endif; ?>
								</div>
							</div>
							<div class="home wow">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512px" height="512px">
									<g>
										<g>
											<path d="M506.555,208.064L263.859,30.367c-4.68-3.426-11.038-3.426-15.716,0L5.445,208.064 c-5.928,4.341-7.216,12.665-2.875,18.593s12.666,7.214,18.593,2.875L256,57.588l234.837,171.943c2.368,1.735,5.12,2.57,7.848,2.57    c4.096,0,8.138-1.885,10.744-5.445C513.771,220.729,512.483,212.405,506.555,208.064z" fill="#FFFFFF"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M442.246,232.543c-7.346,0-13.303,5.956-13.303,13.303v211.749H322.521V342.009c0-36.68-29.842-66.52-66.52-66.52    s-66.52,29.842-66.52,66.52v115.587H83.058V245.847c0-7.347-5.957-13.303-13.303-13.303s-13.303,5.956-13.303,13.303v225.053    c0,7.347,5.957,13.303,13.303,13.303h133.029c6.996,0,12.721-5.405,13.251-12.267c0.032-0.311,0.052-0.651,0.052-1.036v-128.89    c0-22.009,17.905-39.914,39.914-39.914s39.914,17.906,39.914,39.914v128.89c0,0.383,0.02,0.717,0.052,1.024    c0.524,6.867,6.251,12.279,13.251,12.279h133.029c7.347,0,13.303-5.956,13.303-13.303V245.847    C455.549,238.499,449.593,232.543,442.246,232.543z" fill="#FFFFFF"/>
										</g>
									</g>
								</svg>
								<div class="text" style="color:<?php echo $colortext; ?>;">
									<?php the_sub_field('adres'); ?>
								</div>
							</div>
							<div class="text mt-4 w-100" style="color:<?php echo $colortext; ?>;">
								<?php the_sub_field('informacje_dodatkowe'); ?>
							</div>
						</div>
					</div>
					<div class="col-xl-6">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="col-xl-12 my-5">
					<?php if (get_sub_field( 'mapa_google' )): ?>
						<iframe src="<?php the_sub_field( 'mapa_google' ); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<?php endif; ?>
				</div>
			</div>
		</section>
