<?php 
if (get_sub_field("ulozenie") == 1) {
	add_css_theme("contact-1-css", "/templates/contact/contact-1/scss/style.css");
	get_template_part( 'templates/contact/contact-1/contact'); 
}
elseif (get_sub_field("ulozenie") == 2) {
	add_css_theme("contact-2-css", "/templates/contact/contact-2/scss/style.css");
	get_template_part( 'templates/contact/contact-2/contact'); 
}
elseif (get_sub_field("ulozenie") == 3) {
	add_css_theme("contact-3-css", "/templates/contact/contact-3/scss/style.css");
	get_template_part( 'templates/contact/contact-3/contact'); 
}
