<section class="textbox" id="<?php the_sub_field("id_sekcji"); ?>" style="background: <?php echo $hn_bgcolor ?>">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground ?>); opacity: <?php echo $hn_opacity; ?>"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="<?php echo $hn_rwd ?>">
			<div class="row">
				<div class="description" style="color:<?php echo $hn_fcolor ?>">
					<?php the_sub_field("text"); ?>
				</div>
			</div>
		</div>
	</div>
</section> 