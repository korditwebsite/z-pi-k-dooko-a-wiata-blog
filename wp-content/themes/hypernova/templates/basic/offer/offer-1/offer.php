<section class="oferta-1" id="oferta" style="background-color:<?php echo $hn_bgcolor; ?>;">
	<div class="<?php echo $hn_size_container; ?>">
		<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground; ?>); opacity: <?php echo $hn_opacity; ?>;"></div>
		<div class="row">
			<div class="col-xl-12">
				<div class="section-title">
					<h2 class="wow W-100 text-center white flipInX mb-5">Pozostała oferta</h2>
				</div>
			</div>
			<?php  $time = 250; if( have_rows('pojedyncza_oferta') ):  while ( have_rows('pojedyncza_oferta') ) : the_row();  ?>
			<div class="<?php echo $hn_rwd; ?> wow flipInX" data-wow-delay="<?php echo $time; ?>ms">
				<div class="offer-wrapper">
					<div class="offer-box">
						<div class="offer-box-wrapper">
							<div class="thumbnail">
								<img src="<?php echo the_sub_field( 'grafika' ); ?>">
							</div>
							<div class="text-container">
								<h3 style="color:<?php echo $hn_fcolor; ?>;"><?php the_sub_field("tytul"); ?></h3>
								<p style="color:<?php echo $hn_fcolor; ?>;"><?php the_sub_field("opis"); ?></p>
							</div>
						</div>
						<?php if (get_sub_field("link")): ?>
							<div class="href-center">
								<a style="color:<?php echo $hn_scolor; ?>;" href="<?php the_sub_field("link"); ?>" target="_blank">czytaj więcej</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php $time = $time + 250; endwhile; else : endif; ?>
		</div>
	</div>
</section>