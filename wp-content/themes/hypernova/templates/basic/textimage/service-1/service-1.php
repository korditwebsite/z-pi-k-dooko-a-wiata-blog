<section class="service-1" id="<?php the_sub_field("id_sekcji"); ?>" style="background: <?php echo $hn_bgcolor ?>">
	<div class="section-background-image" style="background-image:url(<?php echo $hn_imagebackground ?>); opacity: <?php echo $hn_opacity; ?>"></div>
	<div class="<?php echo $hn_size_container; ?>">
		<div class="row">
			<div class="<?php echo $hn_service_size_image; ?> wow fadeInLeft">
				<h3 style="color:<?php echo $hn_fcolor ?>;"><?php the_sub_field("tytul"); ?></h3>
				<div class="description" style="color:<?php echo $hn_scolor ?>">
					<?php the_sub_field("tekst"); ?>
				</div>
			</div>
			<div class="<?php echo $hn_service_size_text; ?> wow fadeInRight">				
				<div class="row">
					<div class="col-xl-6">
						<div class="container-image">
							<div class="w-100 single-image wow fadeInUp">
								<?php
								echo wp_get_attachment_image( get_sub_field('grafika_3'), "about-small-1", "", array( "class" => "w-100 img-fluid") );
								?>
							</div>
							<div class="w-100 single-image wow fadeInDown">
								<?php
								echo wp_get_attachment_image( get_sub_field('grafika_2'), "about-small-1", "", array( "class" => "w-100 img-fluid") );
								?>		
							</div>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="wowfadeInRight">
							<?php
							echo wp_get_attachment_image( get_sub_field('grafika'), "about-small-1", "", array( "class" => "w-100 img-fluid") );
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> 