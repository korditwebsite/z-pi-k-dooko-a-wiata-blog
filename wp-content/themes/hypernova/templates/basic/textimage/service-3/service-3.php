<section class="service-3" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-md-12 col-12 wow fadeInLeft">
				<div class="content service-3-box">
					<p><?php the_sub_field("tekst"); ?></p>
				</div>
			</div>
			<div class="col-xl-6 col-md-12 col-12 wow fadeInRight">
				<div class="thumbnail responsive">
					<?php $grafika = wp_get_attachment_image_url( get_sub_field('grafika'), "kontener" ); ?>
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy about-img img-fluid", "data-src=" => $grafika ) );  ?>
				</div>
			</div>
		</div>
	</div>
</section>