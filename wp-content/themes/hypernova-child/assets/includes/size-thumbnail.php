<?php
//Rozmiar obrazków
add_theme_support('post-thumbnails');
add_image_size( 'malyprostokat', 400, 300 );
add_image_size( 'o-nas', 740, 520 );
add_image_size( 'kontener', 1100, 630 );
add_image_size( 'big', 1024, 1024 );
add_image_size( 'full-gallery', 1920, 780 );
add_image_size( 'full', 1920, 1080 );
add_image_size( 'paralax-home', 680, 1110 );
add_image_size( 'gallery', 520, 380 );
add_image_size( 'gallery-home', 500, 400);
add_image_size( 'small-logo', 320, 100);