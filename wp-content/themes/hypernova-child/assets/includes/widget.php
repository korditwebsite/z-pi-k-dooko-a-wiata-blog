<?php 
//Inicjacja Sidebar
function quality_construction_widgets_init()
{
	register_sidebar(array(
		'name' => esc_html__('Sidebar', 'quality-construction'),
		'id' => 'sidebar-1',
		'description' => esc_html__('Add widgets here.', 'quality-construction'),
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'quality_construction_widgets_init');