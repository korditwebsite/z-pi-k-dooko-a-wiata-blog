<?php 

function service_taxonomy() {
            //START DEFINE VARIABLE
    $nametaxonomy = "test123";
    $test = mb_strtolower($nametaxonomy);
    $search = explode(",","ą,ć,ę,ł,ń,ó,ś,ź,ż");
    $replace = explode(",","a,c,r,l,n,o,s,z,z");
    $urlTitle = str_replace($search, $replace, $test);

            //STOP DEFINE VARIABLE
    $labels = array(
        'name' => _x( $nametaxonomy, 'taxonomy general name' ),
        'singular_name' => _x( $nametaxonomy, 'taxonomy singular name' ),
        'search_items' =>  __( 'Wyszukaj ' . $nametaxonomy ),
        'all_items' => __( 'Wszystkie ' . $nametaxonomy ),
        'edit_item' => __( 'Edytuj ' . $nametaxonomy ), 
        'update_item' => __( 'Uaktualnij ' . $nametaxonomy ),
        'add_new_item' => __( 'Dodaj ' . $nametaxonomy ),
        'new_item_name' => __( 'Nowe ' . $nametaxonomy ),
    );    

    register_taxonomy("test123",array("oferta"), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'menu_icon'   => 'dashicons-images-alt2',
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => $urlTitle ),
    ));
}

// Taksomia popularne
add_action( 'init', 'service_taxonomy', 0 );