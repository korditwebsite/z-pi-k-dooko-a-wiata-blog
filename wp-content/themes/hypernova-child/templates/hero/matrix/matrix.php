<?php 
$background = wp_get_attachment_image_url( get_sub_field('background'), "full" );
$bgcolor = get_sub_field( 'background_color' );
$opacity = get_sub_field( 'opacity' );
$text = get_sub_field( 'main_text' );
$logo = wp_get_attachment_image( get_sub_field('logo'), "full" );
$vachromatic = get_sub_field( 'monochrome' );
$chromatic = "color-" . strtolower($vachromatic);
js_matrix(); //uruchamia funkcję w assets support, która powoduje aktywacje bibliotek odpowiedzialnych za animowanie slidera.
?>
<section class="matrix" style="background-color: <?php echo $bgcolor; ?>;">
	<div class="container-matrix">
		<div class="content">
			<div class="background" style="background-image: url(<?php echo $background; ?>); opacity: <?php echo $opacity; ?>"></div>
			<div class="content__title animation-slider">
				<div class="logotype-present animated zoomInUp" data-wow-delay="0.5s">
					<span class="inner-animation">
						<?php echo $logo; ?>
					</span>
				</div>
				<p class="content__tagline wow zoomInUp" data-wow-delay="2s"><?php echo $text; ?></p>
				
				<a class="href-matrix" href="/kontakt">
					<span class="contact text-matrix">Kontakt</span>
				</a>
				<a class="href-matrix" href="#about-us">
					<span class="read-more text-matrix">Czytaj dalej</span>
				</a>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var imageUrl = "<?php echo $background; ?>";
		jQuery(document).ready(function($){ 
			$('.background__copy').css('background-image', 'url("' + imageUrl + '")');
		});
	</script>
</section>