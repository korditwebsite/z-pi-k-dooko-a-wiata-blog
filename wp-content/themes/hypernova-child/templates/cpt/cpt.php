 <section id="blog-page">
 	<div class="container">
 		<div class="row">
 			<div class="col-xl-12">
 				<div class="title-container">
 					<h3><?php the_sub_field( 'title' ); ?></h3>
 					<p><?php the_sub_field( 'subtitle' ); ?></p>
 				</div>
 			</div>
 		</div>
 		<div class="row">
 			<?php
 			$namefield = get_sub_field( 'category_name' );
 			$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
 			$args = array(
 				'post_type'   => $namefield,
 				'post_status' => 'publish',
 				'order' => 'ASC'
 			);
 			?>
 			<?php $testimonials = new WP_Query( $args ); 
 			if( $testimonials->have_posts() ) :
 				?>
 				<?php
 				while( $testimonials->have_posts() ) :
 					$testimonials->the_post();
 					?>
 					<div class="col-xl-4">
 						<div class="inner-post-list">
 							<div class="thumbnail">
 								<?php the_post_thumbnail( 'category-thumb' ); ?>
 							</div>
 							<div class="inner-content">
 								<div class="link">
 									<a href="<?php echo get_permalink(); ?>">
 										<?php the_title(); ?>
 									</a>
 								</div>
 							</div>
 						</div>
 					</div>
 					<?php
 				endwhile;
 				wp_reset_postdata();
 				?>
 				<?php
 			else :
 				esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
 			endif;
 			?>
 		</div>
 	</div>
 </section>